import React, { Component } from 'react'
import  { Link } from 'react-router-dom';

class UpdateMovie extends Component {
    
    state={
        Title:'',
        Description:'',
        Runtime:'',
        Genre:'',
        Rating:'',
        Metascore:'',
        Votes:'',
        Gross_Earning_in_Mil:'',
        Director:'',
        Actor:'',
        Year:'',
    }

    async componentDidMount() {
        const id = this.props.match.params.id
        await fetch(`http://localhost:8000/api/movies/${id}`)
            .then(response => response.json())
            .then(data => {
                // console.log(data)
                this.setState({
                    Title:data.Title,
                    Description:data.Description,
                    Runtime:data.Runtime,
                    Genre:data.Genre,
                    Rating:data.Rating,
                    Metascore:data.Metascore,
                    Votes:data.Votes,
                    Gross_Earning_in_Mil:data.Gross_Earning_in_Mil,
                    Director:data.Director,
                    Actor:data.Actor,
                    Year:data.Year,
                });
                // console.log(this.state)
            })
    }


    changeHandler=(e) => {
        // console.log(e.target.value);
        // console.log(this.props.match.params.id)
        this.setState({[e.target.name]:e.target.value})
    }

    onSubmit = async (e) => {
        e.preventDefault();
        const body=this.state;
        // console.log(this.state.Title)
        // console.log('ji')
        const url = `http://localhost:8000/api/movies/${this.props.match.params.id}`;
        await fetch(url, {
            method: 'PUT',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        })
        .then(res =>{
            if(res.ok){
                this.props.history.push(`/movie_info/${this.props.match.params.id}`);
            }
        })
    }

    render() {
        return (
            <div className='addDirectorContainer'>
                <header className='navbar'>Edit_movie_Detail</header>             
            <form onSubmit={this.onSubmit}>
            <div className ='movie-info'>
            <div className='singleMoviesBox'>
                <label>Title: </label>
                <input name="Title"
                type="text"
                value={this.state.Title}
                onChange ={this.changeHandler}></input>
                </div>
                

                <div className='singleMoviesBox'>
                <label>Description: </label>
                <input name="Description"
                type="text"
                value={this.state.Description}
                onChange ={this.changeHandler}></input>
                </div>
               
                

                <div className='singleMoviesBox'>
                <label>Director: </label>
                <input name="Director"
                type="text"
                value={this.state.Director}
                onChange ={this.changeHandler}></input>
                </div>
                
                
                <div className='singleMoviesBox'>
                <label>Genre: </label>
                <input name="Genre"
                value={this.state.Genre}
                onChange ={this.changeHandler}></input>
                </div>
                
                <div className='singleMoviesBox'>
                <label>Year: </label>
                <input name="Year"
            
                value={this.state.Year}
                onChange ={this.changeHandler}></input>
                </div>
                
                
                <div className='singleMoviesBox'>
                <label>Gross Earning in Mil: </label>
                <input name="Gross_Earning_in_Mil"
              
                value={this.state.Gross_Earning_in_Mil}
                onChange ={this.changeHandler}></input>
                </div>
                
                
                <div className='singleMoviesBox'>
                <label>Metascore: </label>
                <input name="Metascore"
              
                value={this.state.Metascore}
                onChange ={this.changeHandler}></input>
                </div>
                
               
                <div className='singleMoviesBox'>
                <label>Rating: </label>
                <input name="Rating"
             
                value={this.state.Rating}
                onChange ={this.changeHandler}></input>
                </div>
                
                
                <div className='singleMoviesBox'>
                <label>Runtime: </label>
                <input name="Runtime"
             
                value={this.state.Runtime}
                onChange ={this.changeHandler}></input>
                </div>
                
                <div className='singleMoviesBox'>
                <label>Votes: </label>
                <input name="Votes"
                value={this.state.Votes}
                onChange ={this.changeHandler}></input>
                </div>
                
                
                <div className='singleMoviesBox'>
                    <label>Actor: </label>
                    <input name="Actor"
                    type="text"
                    value={this.state.Actor}
                    onChange ={this.changeHandler}></input>
                </div>
                
                </div>
                <button type='submit' className='btn small-btn' >Update</button>
                <Link to={`/movie_info/${this.props.match.params.id}`}>
                    <button type='submit' className='btn small-btn'>Back</button>
                </Link>

               
                
            </form>
            
            
        </div>
        )
    }
}

export default UpdateMovie
