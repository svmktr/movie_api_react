import React, { Component } from "react";
// import Movies from "./MovieList";
import { Link } from "react-router-dom";
// import Movie_info from "../components/Movie_info";

export class Movie extends Component {
  state = {
    items: []
  };

  async componentDidMount() {
    const url = "http://localhost:8000/api/movies";
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          items: data
        });
      });
  }


  render() {
    // console.log(items)
    let { items } = this.state;
    if (!items) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="Movie-page">
          <header className="navbar">MOVIES</header>
          {/* <h1>Movies List</h1> */}
          <div className='section'>
          <Link to="/addMovie">
            <button className="btn add-btn">+Add Movie</button>
          </Link>

          <Link to="/">
            <button className="btn back-btn small-btn">Back</button>
          </Link>

          <ul className="list">
            {this.state.items.map(item => (
              <Link to={`/movie_info/${item.Rank}`} key={item.Rank}>
                <li className='title' >{item.Title}</li>       
              </Link>
            ))}
          </ul>
          </div>
        </div>
      );
    }
  }
}

export default Movie;
