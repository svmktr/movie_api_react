import React, { Component } from 'react'
import  { Link } from 'react-router-dom';

class AddDir extends Component {
  state={ 
        director:'',      
    }



    changeHandler=(e) => {
        // e.preventDefault()
        // console.log(e.target.value)
        this.setState({[e.target.name]:e.target.value})
        // console.log(this.state)
    }

    onSubmit = async(e) =>{
        e.preventDefault();
        // console.log(this.state.director);
        const body=this.state;
        // console.log(body.Title);
        let flag = true;
        for (let i in body){
            // console.log(body[i])
            // console.log(Number.isInteger(body[i]))
            if (body[i] === ''){
                flag = false;
            }
        }
        if (flag === true){
        const url = 'http://localhost:8000/api/directors';
        await fetch(url, {
            method: 'POST',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        })  
        // .then(response => response.json())
        .then(res => {
            if(res.ok){
            alert('New Director has been added to the List')
            this.props.history.push("/dir");
            }
            else{
                alert('Enter correct data')
            }
        })
        .catch(error =>{
            console.log(error)
        })
        
    }   
    else{
        alert('Fill up all the fields')
        // setTimeout(function() { alert("my message"); }, 3000) ;
        // document.write ("This is a warning message!");
        // var dialog = dialog('open');
// setTimeout(function() { dialog.dialog('close'); }, 2000);
    }
    }

    render() {
        return (
            <div className='addDirectorContainer'>
                <header className='navbar'>Add_New_Director</header>
                <form onSubmit={this.onSubmit} className='moviesForm'>

                <div className ='movie-info dir-Edit'>

                    <div className='singleMoviesBox'>
                    <label>Director :</label>
                    <input className='input-dir'
                    name="director"
                    value={this.state.director}
                    onChange ={this.changeHandler}></input>
                    </div>
                 
                    </div>
                    <div>
                    <button type='submit' className='btn small-btn'  >+Add </button>
                    <Link to='/dir'>
                        <button type='submit' className='btn small-btn'  >Back </button>
                    </Link>
                    </div>
                    
                </form>
                
            </div>
        )
    }
}

export default AddDir
