import React, { Component } from "react";
// import Movies from "./MovieList";
import { Link } from "react-router-dom";
// import Movie_info from "../components/Movie_info";

export class Director extends Component {
  state = {
    items: []
  };

  async componentDidMount() {
    const url = "http://localhost:8000/api/directors";
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          items: data
        });
      });
  }


  render() {
    // console.log(items)
    let { items } = this.state;
    if (!items) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="Director-page">
          <header className="navbar">Director</header>
          <div className='section'>
          <Link to='/dir/addDir'>
            <button className="btn add-btn">+ Director</button>
          </Link>

          <Link to="/">
            <button className="btn back-btn small-btn">Back</button>
          </Link>

          <ul className="list">
            {this.state.items.map(item => (
              <Link to={`/dir/dir_info/${item.id}`} key={item.id}>
                {/* {console.log(item.id)} */}
                <li className='title' >{item.director}</li>      
              </Link>
            ))}
          </ul>

          </div>
        </div>
      );
    }
  }
}

export default Director;
