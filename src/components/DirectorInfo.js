import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export class DirectorInfo extends Component {
    state = {
        Id:'',
        Director: ''     
    }


    async componentDidMount() {
        const id = this.props.match.params.id
        const url = `http://localhost:8000/api/directors/${id}`;
        await fetch(url)
            .then(res => res.json())
            .then(data => {
                // console.log(data)
                this.setState({
                    Id:data.id,
                    Director: data.director
                });
            });
    }

    delete = async (id) => {
        const url = `http://localhost:8000/api/directors/${id}`;
        await fetch(url, {
            method: "DELETE"
        }).then(alert("Director has been deleted"))
        .then(this.props.history.push("/dir"));
    };
 

    render() {
        const Rank = this.state.Id
        return (
            <div>
                <header className='navbar'>Dir_Info</header>
                <div className='movie-info-block info-block'>
                <ul className='dir-info'>
                     <li>Name: {this.state.Director}</li>
                </ul>
                <Link to='/dir'>
                    <button className='btn small-btn' >Back</button>
                </Link>
                
                <button className='btn small-btn'  onClick={this.delete.bind(this, Rank)} >Delete</button>

                <Link to={`/dir-update/${Rank}`}>
                    <button className='btn small-btn' >Edit</button>
                </Link>
                </div>
            </div>
        )
    }
}

export default DirectorInfo;