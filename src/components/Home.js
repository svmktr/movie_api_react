import React, { Component } from 'react'
import {Link} from  'react-router-dom';
import Header from '../components/Header'


class Home extends Component {
    render() {
        return (
            <div>
                <Header/>
                <div className = 'movie-dir'>
                <Link to = '/movie'>
                <button className='btn'>Movies</button>
                </Link>

                <Link to = '/dir'>
                <button className='btn'>Directors</button>
                </Link>
                </div>
            </div>
        )
    }
}

export default Home
