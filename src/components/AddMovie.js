import React, { Component } from 'react'
import { Link } from 'react-router-dom';

class AddMovie extends Component {
    state = {
        Title: '',
        Description: '',
        Runtime: '',
        Genre: '',
        Rating: '',
        Metascore: '',
        Votes: '',
        Gross_Earning_in_Mil: '',
        Director: '',
        Actor: '',
        Year: '',
    }



    changeHandler = (e) => {
        // e.preventDefault()
        // console.log(e.target.value)
        this.setState({ [e.target.name]: e.target.value })
        // console.log(this.state)
    }

    onSubmit = async (e) => {
        e.preventDefault();
        // console.log(this.state.director);
        const body = this.state;
        // console.log(body.Title);
        let flag = true;
        for (let i in body) {
            // console.log(typeof(body[i]))
            if (body[i] === '') {
                flag = false;
            }
        }
        if (flag === true) {
            const url = 'http://localhost:8000/api/movies';
            await fetch(url, {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(body),
            })
                // .then(response => response.json())
                .then(res => {
                    if (res.ok) {
                        alert('New movie has been added to the List')
                        this.props.history.push("/movie");
                    }
                    else {
                        alert('Enter correct data')
                    }
                })
                .catch(error => {
                    console.log(error)
                })

        }
        else {
            alert('Fill up all the fields')
            // setTimeout(function() { alert("my message"); }, 3000) ;
            // document.write ("This is a warning message!");
            // var dialog = dialog('open');
            // setTimeout(function() { dialog.dialog('close'); }, 2000);
        }
    }

    render() {
        return (
            <div className='addDirectorContainer'>
                <header className='navbar'>Add_New_Movie</header>
                <form onSubmit={this.onSubmit} className='moviesForm'>
                    <div className='movie-info'>
                        <div className='singleMoviesBox'>
                            <label>Title: </label>
                            <input name="Title"
                                type="text"
                                value={this.state.Title}
                                onChange={this.changeHandler}></input>
                        </div>


                        <div className='singleMoviesBox'>
                            <label>Description: </label>
                            <input name="Description"
                                type="text"
                                value={this.state.Description}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Runtime: </label>
                            <input name="Runtime"
                                value={this.state.Runtime}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Genre: </label>

                            <input name="Genre"
                                value={this.state.Genre}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Rating: </label>
                            <input name="Rating"
                                value={this.state.Rating}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Metascore: </label>
                            <input name="Metascore"
                                value={this.state.Metascore}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Votes: </label>
                            <input name="Votes"
                                value={this.state.Votes}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Gross Earning in Mil: </label>
                            <input name="Gross_Earning_in_Mil"
                                value={this.state.Gross_Earning_in_Mil}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Director: </label>
                            <input name="Director"
                                type="text"
                                value={this.state.Director}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Actor: </label>
                            <input name="Actor"
                                type="text"
                                value={this.state.Actor}
                                onChange={this.changeHandler}></input>
                        </div>

                        <div className='singleMoviesBox'>
                            <label>Year: </label>
                            <input name="Year"
                                value={this.state.Year}
                                onChange={this.changeHandler}></input>
                        </div>
                    </div>

                    <div>
                        <button type='submit' className='btn small-btn' > +AddMovie </button>
                        <Link to='/'>
                            <button type='submit' className='btn small-btn' >Back </button>
                        </Link>
                    </div>

                </form>

            </div>
        )
    }
}

export default AddMovie
