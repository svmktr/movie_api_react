import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export class Movie_info extends Component {
    state = {
        Rank:'',
        Title: '',
        Description: '',
        Runtime: '',
        Genre: '',
        Rating: '',
        Metascore: '',
        Votes: '',
        Gross_Earning_in_Mil: '',
        Director: '',
        Actor: '',
        Year: '',
    }


    async componentDidMount() {
        const id = this.props.match.params.id
        const url = `http://localhost:8000/api/movies/${id}`;
        await fetch(url)
            .then(res => res.json())
            .then(data => {
                // console.log(data)
                this.setState({
                    Rank:data.Rank,
                    Title: data.Title,
                    Description: data.Description,
                    Runtime: data.Runtime,
                    Genre: data.Genre,
                    Rating: data.Rating,
                    Metascore: data.Metascore,
                    Votes: data.Votes,
                    Gross_Earning_in_Mil: data.Gross_Earning_in_Mil,
                    Director: data.Director,
                    Actor: data.Actor,
                    Year: data.Year
                });
            });
    }

    delete = async (id) => {
        // console.log(id);
        const url = `http://localhost:8000/api/movies/${id}`;
        await fetch(url, {
            method: "DELETE"
        }).then(alert("movie has been deleted"))
        .then(this.props.history.push("/movie"));
    };
 

    render() {
        const Rank = this.state.Rank
        return (
            <div>
                <header className='navbar'>Movie_Info</header>
                <div className='movie-info-block info-block'>
                <ul className='movie-info'>
                     <li>Title : {this.state.Title}</li>
                     <li>Description : {this.state.Description}</li>
                     <li>Director : {this.state.Director}</li>
                     <li>Genre : {this.state.Genre}</li>
                     <li>Year : {this.state.Year}</li>
                     <li>Gross_Earning_in_Mil : {this.state.Gross_Earning_in_Mil}</li>
                     <li>Metascore : {this.state.Metascore}</li>
                     <li>Rating : {this.state.Rating}</li>
                     <li>Runtime : {this.state.Runtime}</li>
                     <li>Votes : {this.state.Votes}</li>
                     <li>Actor : {this.state.Actor}</li>
                </ul>
                
                <button className='btn small-btn' onClick={this.delete.bind(this, Rank)} >Delete</button>
                
                <Link to={`/movieUpdate/${Rank}`}>
                    <button className='btn small-btn '>Edit</button>
                </Link>

                <Link to='/movie'>
                    <button className='btn small-btn movie-info-back-btn'>Back</button>
                </Link>
                </div>
            </div>
        )
    }
}

export default Movie_info;