import React, { Component } from 'react'
import  { Link } from 'react-router-dom';

class UpdateDir extends Component {
    
    state={
        director:''
    }

    async componentDidMount() {
        const id = this.props.match.params.id
        await fetch(`http://localhost:8000/api/directors/${id}`)
            .then(response => response.json())
            .then(data => {
                // console.log(data)
                this.setState({
                    // Id:data.id,
                    director:data.director
                });
                // console.log(this.state)
            })
    }


    changeHandler=(e) => {
        // console.log(e.target.value);
        // console.log(this.props.match.params.id)
        this.setState({[e.target.name]:e.target.value})
    }

    onSubmit = async (e) => {
        e.preventDefault();
        const id = this.props.match.params.id;
        // console.log(id)
        const body=this.state;
        // console.log(this.state)
        // console.log('ji')
        const url = `http://localhost:8000/api/directors/${id}`;
        await fetch(url, {
            method: 'PUT',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        })
        .then(res =>{
            if(res.ok){
                this.props.history.push(`/dir/dir_info/${this.props.match.params.id}`);
            }
        })
    }

    render() {
        return (
            <div className='addDirectorContainer'>
             <header className='navbar'>Edit_Director</header>             
            <form onSubmit={this.onSubmit}>

            
                <div className ='movie-info dir-Edit'>
                <label>Director :</label>
                <input  className='input-dir'
                name="director"
                type="text"
                value={this.state.director}
                onChange ={this.changeHandler}></input>
                </div>       
                
             
                <button type='submit' className='btn small-btn' >Update </button>
                <Link to={`/dir/dir_info/${this.props.match.params.id}`}>
                    <button type='submit' className='btn small-btn' >Back </button>
                </Link>

            
                
            </form>
            
        </div>
        )
    }
}

export default UpdateDir
