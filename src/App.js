import React, { Component } from 'react';
import './App.css'
// import Header from '../src/components/Header';
import Movie from '../src/components/Movie';
import UpdateMovie from '../src/components/UpdateMovie';
import AddMovie from '../src/components/AddMovie';
import Home from '../src/components/Home';
import MovieInfo from '../src/components/Movie_info'
import Director from './components/Director';
import DirectorInfo from '../src/components/DirectorInfo'
import DirectorUpdate from '../src/components/UpdateDir'
import AddDir from '../src/components/Add_dir'

import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <Router>
         <Switch>
         <Route path ='/' exact component ={Home} />
         <Route path ='/movie' exact component ={Movie} />
         <Route path ='/movie_info/:id' exact component ={MovieInfo} />
         <Route path ='/movieUpdate/:id' exact component ={UpdateMovie} />   
         <Route path ='/addMovie' exact component ={AddMovie} />  
         <Route path ='/dir' exact component ={Director} />
         <Route path ='/dir/dir_info/:id' exact component ={DirectorInfo} />
         <Route path ='/dir-update/:id' exact component ={DirectorUpdate} />
         <Route path ='/dir/addDir' exact component ={AddDir} />
      

         </Switch>    
        </Router>
    )
  }
}

// export default App



// function App() {
//   return (
//     <div>
//       <Header/>
//       <Main />
//     </div>
//   );
// }

export default App;
